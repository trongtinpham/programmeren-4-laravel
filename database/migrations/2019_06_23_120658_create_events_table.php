<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('events');
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 120);
            $table->char('location', 120);
            $table->datetime('starts')->nullable();
            $table->datetime('ends')->nullable();
            $table->char('image', 255);
            $table->string('description');
            $table->char('organiser_name', 120);
            $table->char('organiser_description', 120);
            $table->integer('eventcategory_id')->unsigned();
            $table->integer('eventtopic_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('eventcategory_id')->references('id')->on('event_categories');
            $table->foreign('eventtopic_id')->references('id')->on('event_topics');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
