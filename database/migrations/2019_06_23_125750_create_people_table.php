<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('people');
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->char('first_name', 50);
            $table->char('last_name', 120);
            $table->char('email', 255)->nullable();
            $table->char('address1', 255)->nullable();
            $table->char('address2', 255)->nullable();
            $table->char('postalcode', 20)->nullable();
            $table->char('city', 80)->nullable();
            $table->integer('country_id')->unsigned();
            $table->char('phone1', 25)->nullable();
            $table->date('birthday')->nullable();
            $table->integer('rating')->nullable();
            $table->timestamps();
            
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('people');
    }
}
