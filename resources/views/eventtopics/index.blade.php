@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Event Topic</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Naam</th>
                    </tr>
                    @foreach($topics as $topic)
                    <tr>
                        <td>{{$topic->name}}</td>
                        <td>
                            {{ link_to_route('event_topic.show', 'Details', [$topic->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
            {{ link_to_route('event_topic.create', 'Nieuwe topic toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection