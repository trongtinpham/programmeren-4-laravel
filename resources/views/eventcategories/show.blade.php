@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Details</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>['event_category.destroy',$category->id], 'method'=>'DELETE')) !!}
                    <div class="form-group">
                        {!! Form::label('name','Naam')!!}
                        {!! Form::text('name', $category->name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    
                    <div class="form-group">
                        {{ link_to_route('event_category.index', 'Terug', null,['class'=>'btn btn-primary']) }}
                        |
                        {{ link_to_route('event_category.edit', 'Wijzig', [$category->id],['class'=>'btn btn-primary']) }}
                        |
                        {!! Form::button('Verwijder', ['class'=>'btn btn-danger','type'=>'submit']) !!}
                        
                    </div>
                    {!! Form::close() !!}
                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
