@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Event Categorie</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Naam</th>
                    </tr>
                    @foreach($categories as $category)
                    <tr>
                        <td>{{$category->name}}</td>
                        <td>
                            {{ link_to_route('event_category.show', 'Details', [$category->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
            {{ link_to_route('event_category.create', 'Nieuwe categorie toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection