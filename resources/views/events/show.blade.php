@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Details</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>['event.destroy',$event->id], 'method'=>'DELETE')) !!}
                    <div class="form-group">
                        {!! Form::label('name','Naam')!!}
                        {!! Form::text('name', $event->name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('location','Locatie')!!}
                        {!! Form::text('location', $event->location, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('starts','Start')!!}
                        {!! Form::text('starts', $event->starts, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('ends','Einde')!!}
                        {!! Form::text('ends', $event->ends, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('image','Afbeelding')!!}
                        {!! Form::text('image', $event->image, ['class'=>'form-control', 'accept' => 'image/*', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Beschrijving')!!}
                        {!! Form::textarea('description', $event->description, ['class'=>'form-control', 'rows' => 5, 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('organiser_name','Organisator naam')!!}
                        {!! Form::text('organiser_name', $event->organiser_name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('organiser_description','Organisator beschrijving')!!}
                        {!! Form::textarea('organiser_description', $event->organiser_description, ['class'=>'form-control', 'rows' => 3, 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('eventcategory_id','Event categorie')!!}
                        {!! Form::text('eventcategory_id', $event->getEventCategoryName(), ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('eventtopic_id','Event topic')!!}
                        {!! Form::text('eventtopic_id', $event->getEventTopicName(), ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    
                    <div class="form-group">
                        {{ link_to_route('event.index', 'Terug', null,['class'=>'btn btn-primary']) }}
                        |
                        {{ link_to_route('event.edit', 'Wijzig', [$event->id],['class'=>'btn btn-primary']) }}
                        |
                        {!! Form::button('Verwijder', ['class'=>'btn btn-danger','type'=>'submit']) !!}
                        
                    </div>
                    {!! Form::close() !!}
                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
