@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Events</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Naam</th>
                        <th>Locatie</th>
                        <th>Afbeelding</th>
                        <th>Beschrijving</th>
                        <th>Organisator naam</th>
                    </tr>
                    @foreach($events as $event)
                    <tr>
                        <td>{{$event->name}}</td>
                        <td>{{$event->location}}</td>
                        <td>{{$event->image}}</td>
                        <td>{{$event->description}}</td>
                        <td>{{$event->organiser_name}}</td>
                        <td>
                            {{ link_to_route('event.show', 'Details', [$event->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
            {{ link_to_route('event.create', 'Nieuw event toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection