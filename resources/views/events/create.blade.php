@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Event toevoegen</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>'event.store')) !!}
                    <div class="form-group">
                        {!! Form::label('name','Naam')!!}
                        {!! Form::text('name', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('location','Locatie')!!}
                        {!! Form::text('location', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('start_date','Start datum')!!}
                        {!! Form::input('date', 'start_date', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('start_time','Start tijd')!!}
                        {!! Form::input('time', 'start_time', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('end_date','Eind datum')!!}
                        {!! Form::input('date', 'end_date', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('end_time','Eind tijd')!!}
                        {!! Form::input('time', 'end_time', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('image','Afbeelding')!!}
                        {!! Form::file('image', ['class'=>'form-file', 'accept' => 'image/*']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Beschrijving')!!}
                        {!! Form::textarea('description', null, ['class'=>'form-control', 'rows' => 5]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('organiser_name','Organisator naam')!!}
                        {!! Form::text('organiser_name', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('organiser_description','Organisator beschrijving')!!}
                        {!! Form::textarea('organiser_description', null, ['class'=>'form-control', 'rows' => 3]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('eventcategory_id','Event categorie')!!}
                        {!! Form::select('eventcategory_id', $categories, null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('eventtopic_id','Event topic')!!}
                        {!! Form::select('eventtopic_id', $topics, null, ['class'=>'form-control']) !!}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::button('Create',['type'=>'submit','class'=>'btn btn-primary'])!!}
                    </div>
                    
                    {!! Form::close() !!}
                
              </div>
            </div>
            @if($errors->has())
                <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
@endsection
