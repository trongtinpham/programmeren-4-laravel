@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Rollen</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Naam</th>
                    </tr>
                    @foreach($roles as $role)
                    <tr>
                        <td>{{$role->name}}</td>
                        <td>
                            {{ link_to_route('role.show', 'Details', [$role->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
            {{ link_to_route('role.create', 'Nieuwe rol toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection