@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Landen</div>
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <th>Naam</th>
                            <th>Code</th>
                        </tr>
                        @foreach($countries as $country)
                        <tr>
                            <td>{{$country->name}}</td>
                            <td>{{$country->code}}</td>
                            <td>
                                {{ link_to_route('country.show', 'Details', [$country->id],['class'=>'btn btn-primary']) }}
                            </td>
                        </tr>
                        @endforeach
                    </table>
              </div>
            </div>
            {{ link_to_route('country.create', 'Nieuw land toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection