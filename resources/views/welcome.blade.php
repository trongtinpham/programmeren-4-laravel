@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welkom</div>

                <div class="panel-body">
                    <!--<a href="{{ action("PersonController@index") }}">Personen</a><br>-->
                    <!--<a href="{{ action("CountryController@index") }}">Landen</a><br>-->
                    <!--<a href="{{ action("RoleController@index") }}">Rollen</a><br>-->
                    <!--<a href="{{ action("EventController@index") }}">Events</a><br>-->
                    <!--<a href="{{ action("EventCategoryController@index") }}">Event categorie</a><br>-->
                    <!--<a href="{{ action("EventTopicController@index") }}">Event topics</a><br>-->
                    <!--<a href="{{ action("UserController@index") }}">Gebruikers</a>-->
                    <main>
                        <a href="{{ action("PersonController@index") }}" class="tile"><span>Personen</span></a>
                        <a href="{{ action("CountryController@index") }}"class="tile"><span>Landen</span></a>
                        <a class="tile"></a>
                        <a class="tile"></a>
                        <a class="tile"></a>
                        <a href="{{ action("RoleController@index") }}"class="tile"><span>Rollen</span></a>
                        <a href="{{ action("UserController@index") }}" class="tile"><span>Gebruikers</span></a>
                        <a class="tile"></a>
                        <a href="{{ action("EventController@index") }}" class="tile"><span>Events</span></a>
                        <a href="{{ action("EventCategoryController@index") }}" class="tile"><span>Event Categorie</span></a>
                        <a href="{{ action("EventTopicController@index") }}" class="tile"><span>Event Topic</span></a>
                        <a class="tile"></a>
                    </main>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
