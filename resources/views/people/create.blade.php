@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Persoon toevoegen</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>'person.store')) !!}
                    <div class="form-group">
                        {!! Form::label('first_name','Voornaam')!!}
                        {!! Form::text('first_name', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('last_name','Achternaam')!!}
                        {!! Form::text('last_name', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email')!!}
                        {!! Form::text('email', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('address1','Adres 1')!!}
                        {!! Form::text('address1', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('address2','Adres 2')!!}
                        {!! Form::text('address2', null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('postalcode','Postcode')!!}
                        {!! Form::text('postalcode', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('city','Stad')!!}
                        {!! Form::text('city', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('country','Land')!!}
                        {!! Form::select('country_id', $countries, null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone1','Telefoon')!!}
                        {!! Form::text('phone1', null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('birthday','Geboortedatum')!!}
                        {!! Form::date('birthday', null, ['class'=>'form-control']) !!}
                    </div>
                    
                    <div class="form-group">
                        {!! Form::button('Create',['type'=>'submit','class'=>'btn btn-primary'])!!}
                        
                    </div>
                    {!! Form::close() !!}
                
              </div>
            </div>
            @if($errors->has())
                <ul class="alert alert-danger">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
@endsection
