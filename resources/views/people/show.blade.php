@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Details</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>['person.destroy',$person->id], 'method'=>'DELETE')) !!}
                    <div class="form-group">
                        {!! Form::label('first_name','Voornaam')!!}
                        {!! Form::text('first_name', $person->first_name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('last_name','Achternaam')!!}
                        {!! Form::text('last_name', $person->last_name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email')!!}
                        {!! Form::text('email', $person->email, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('address1','Adres 1')!!}
                        {!! Form::text('address1', $person->address1, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('address2','Adres 2')!!}
                        {!! Form::text('address2', $person->address2,['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('postalcode','Postcode')!!}
                        {!! Form::text('postalcode', $person->postalcode, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('city','Stad')!!}
                        {!! Form::text('city', $person->city, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('country','Land')!!}
                        {!! Form::text('country', $person->getCountryName(), ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone1','Telefoon')!!}
                        {!! Form::text('phone1', $person->phone1, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('birthday','Geboortedatum')!!}
                        {!! Form::date('birthday', $person->birthday, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    
                    <div class="form-group">
                        {{ link_to_route('person.index', 'Terug', null,['class'=>'btn btn-primary']) }}
                        |
                        {{ link_to_route('person.edit', 'Wijzig', [$person->id],['class'=>'btn btn-primary']) }}
                        |
                        {!! Form::button('Verwijder', ['class'=>'btn btn-danger','type'=>'submit']) !!}
                        
                    </div>
                    {!! Form::close() !!}
                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
