@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Personen</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Voornaam</th>
                        <th>Achternaam</th>
                    </tr>
                    @foreach($people as $person)
                    <tr>
                        <td>{{$person->first_name}}</td>
                        <td>{{$person->last_name}}</td>
                        <td>
                            {{ link_to_route('person.show', 'Details', [$person->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
            {{ link_to_route('person.create', 'Nieuw persoon toevoegen', null,['class'=>'btn btn-success']) }}
        </div>
    </div>
</div>
@endsection