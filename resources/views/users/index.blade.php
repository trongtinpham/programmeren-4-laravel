@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Gebruikers</div>

                <div class="panel-body">
                    
                    <table class="table">
                    <tr>
                        <th>Naam</th>
                        <th>Zout</th>
                        <th>Hash</th>
                    </tr>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->salt}}</td>
                        <td>{{isset($user->password) ? 'true' : 'false'}}</td>
                        <td>
                            {{ link_to_route('user.show', 'Details', [$user->id], ['class'=>'btn btn-primary']) }}
                        </td>
                    </tr>
                    @endforeach
                 </table>
                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection