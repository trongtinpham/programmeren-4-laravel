@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Details</div>

                <div class="panel-body">
                    
                    {!! Form::open(array('route'=>['user.destroy',$user->id], 'method'=>'DELETE')) !!}
                    <div class="form-group">
                        {!! Form::label('name','Naam')!!}
                        {!! Form::text('name', $user->name, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email','Email')!!}
                        {!! Form::text('email', $user->email, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('salt','Zout')!!}
                        {!! Form::text('salt', $user->salt, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password','Hash')!!}
                        {!! Form::text('password', $user->password, ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('role_id','Rol')!!}
                        {!! Form::text('role_id', $user->getRoleName(), ['class'=>'form-control', 'readonly' => true]) !!}
                    </div>
                    
                    <div class="form-group">
                        {{ link_to_route('user.index', 'Terug', null,['class'=>'btn btn-primary']) }}
                        |
                        {{ link_to_route('user.edit', 'Wijzig', [$user->id],['class'=>'btn btn-primary']) }}
                        |
                        {!! Form::button('Verwijder', ['class'=>'btn btn-danger','type'=>'submit']) !!}
                        
                    </div>
                    {!! Form::close() !!}
                
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
