<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;
use App\Http\Requests;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = EventTopic::orderBy('name')->get();
        return view('eventtopics.index', compact('topics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventtopics.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        EventTopic::create($request->all());
        return redirect()->route('event_topic.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topic = EventTopic::where('id', $id)->firstOrFail();
        return view('eventtopics.show',compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $topic = EventTopic::where('id', $id)->firstOrFail();
        return view('eventtopics.edit',compact('topic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topic = EventTopic::where('id', $id)->firstOrFail();
        $topic->update($request->all());
        return redirect()->route('event_topic.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = EventTopic::where('id', $id)->firstOrFail();
        $topic->delete();
        return redirect()->route('event_topic.index');
    }
}
