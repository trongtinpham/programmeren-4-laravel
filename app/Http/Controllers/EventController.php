<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;
use App\EventTopic;
use App\Event;
use App\Http\Requests;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('name')->get();
        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = EventCategory::pluck('name', 'id');
        $topics = EventTopic::pluck('name', 'id');
        return view('events.create', compact('categories', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event;
        $event->fill($request->all());
        $event->setStarts($request->start_date, $request->start_time);
        $event->setEnds($request->end_date, $request->end_time);
        
        $event->save();
        return redirect()->route('event.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::where('id', $id)->firstOrFail();
        return view('events.show', compact('event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::where('id', $id)->firstOrFail();
        $categories = EventCategory::pluck('name', 'id');
        $topics = EventTopic::pluck('name', 'id');
        return view('events.edit', compact('event', 'categories', 'topics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::where('id', $id)->firstOrFail();
        $event->fill($request->all());
        $event->setStarts($request->start_date, $request->start_time);
        $event->setEnds($request->end_date, $request->end_time);
        
        $event->save();
        return redirect()->route('event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::where('id', $id)->firstOrFail();
        $event->delete();
        return redirect()->route('event.index');
    }
}
