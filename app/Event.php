<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name', 'location', 'starts', 'ends', 'image', 'description', 'organiser_name', 'organiser_description', 'eventcategory_id', 'eventtopic_id'];
    
    public function eventcategory()
    {
        return $this->belongsTo(EventCategory::class);
    }
    
    public function eventtopic()
    {
        return $this->belongsTo(EventTopic::class);
    }
    
    public function setStarts($date, $time)
    {
        $this->starts = $this->mergeDateTime($date, $time);
    }
    
    public function setEnds($date, $time)
    {
        $this->ends = $this->mergeDateTime($date, $time);
    }
    
    public function getEventCategoryName()
    {
        $category = EventCategory::where('id', $this->eventcategory_id)->firstOrFail();
        return $category->name;
    }
    
    public function getEventTopicName()
    {
        $topic = EventTopic::where('id', $this->eventtopic_id)->firstOrFail();
        return $topic->name;
    }
    
    public function splitDateTime($value, $type)
    {
        $datetime = new \DateTime($value);
        
        if ($type == 'date')
        {
            return $datetime->format('Y-m-d');
        }
        elseif ($type == 'time')
        {
            return $datetime->format('H:i');
        }
        else
        {
            return false;
        }
    }
    
    public function mergeDateTime($date, $time)
    {
        return date('Y-m-d H:i:s', strtotime("$date $time"));
    }
}
