<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name','code'];
    
    public function persons()
    {
        return $this->hasMany(Person::class);
    }
}
