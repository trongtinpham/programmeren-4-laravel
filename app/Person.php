<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = ['first_name', 'last_name', 'email', 'address1', 'address2', 'postalcode', 'city', 'country_id', 'phone1', 'birthday', 'rating'];
    
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    
    public function getCountryName()
    {
        $country = Country::where('id', $this->country_id)->firstOrFail();
        return $country->name;
    }
}